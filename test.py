from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -13 13",
 "p: constant 20000",
 "eta: flat -0.7 0.7",
 "phi: flat -3.14159 3.14159"
 ]


from EvgenJobTransforms.EvgenConfig import evgenConfig

evgenConfig.generators += [ "ParticleGenerator" ]
evgenConfig.description = "simpleJobOption"
